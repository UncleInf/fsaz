<?php 

	include ('php/header.php');
	include ('php/connect.php');
	include ('php/functions.php');


	$admin = 1;

	if (!$admin) {

		echo 'Please <a href=#>login</a> to administer this awesome tool.';

	} else {

		//is viso db irasu
		$all = $db->query("SELECT * FROM FSAZ");
		$problems = $all->fetchAll(PDO::FETCH_ASSOC);
		$count_prob = count($problems);

		//rooms column, skirtingu db irasu
		$all = $db->query("SELECT * FROM FSAZ");
		$rooms = $all->fetchAll(PDO::FETCH_COLUMN, '1');
		$unique_r = array_unique($rooms);
		$count_rooms = count($unique_r);

		//pagal irasu skaiciu koks kabas daugiausiai ir kiek
		$sorted = array_count_values($rooms);
		$most_room = array_keys($sorted);
		$room_probs = array_values($sorted);
		$most_room = $most_room['0'];
		$room_probs = $room_probs['0'];

		//most likes
		$all = $db->query("SELECT * FROM FSAZ ORDER BY svarba DESC");
		$svarba = $all->fetchAll(PDO::FETCH_ASSOC);
		$most_likes = $svarba['0']['svarba'];
		$most_likes_room = $svarba['0']['auditorija'];
		$most_likes_prob = $svarba['0']['problema'];

		//getting first, last dates
		$all = $db->query("SELECT * FROM FSAZ ORDER BY data");
		$data = $all->fetchAll(PDO::FETCH_ASSOC);
		$beginning = $data['0']['data'];
		$end = $data[$count_prob-1]['data'];

		//getting hidden number
		$all = $db->query("SELECT * FROM FSAZ");
		$visible = $all->fetchAll(PDO::FETCH_COLUMN, '5');
		$sorted_visible = array_count_values($visible);
		$hidden_count = array_values($sorted_visible);
		$hidden_count = $hidden_count['1'];

		//auditorijos like suma
		$all = $db->query("SELECT * FROM FSAZ ORDER BY auditorija");
		$rooms = $all->fetchAll(PDO::FETCH_ASSOC);
		$room_likes = array();

		foreach ($rooms as $row) {

			$aud = $row['auditorija'];
			$svarb = $row['svarba'];

			if (!isset($room_likes[$aud])) {
				$room_likes[$aud] = 0;
			}

			$room_likes[$aud] += $svarb;

		}
		arsort($room_likes);
		$most_total_likes_room = array_keys($room_likes);
		$most_total_likes = array_values($room_likes);



		echo '<h3>Statistika</h3>';
		echo '<p>Šiek tiek statistikos iš turimos informacijos.</p> <br/>';
		echo "<div class='panel panel-success'>
				<div class='panel-heading'>
					FSAZ 
					<div class='pull-right'>
						<small rel=tooltip2 title='pirmoji problema buvo užregistruota'>$beginning</small> / 
						<small rel=tooltip2 title='paskutinė problema užregistruota'>$end</small>
					</div>
				</div>";

			echo '<table class="table table-striped"> <tbody>';
				echo "<tr><td><b>Iš viso</b> nusiskundimų <span class=badge rel=tooltip2 title='išviso įrašų db'>$count_prob</span>
						Ūžslėptų nusiskundimų: <span class=badge rel=tooltip2 title='nerodomų visiems įrašų'>$hidden_count</span>
					</td></tr>";
				echo "<tr><td>Turime <span class=badge rel=tooltip2 title='atmetant vienodas auditorijas'>$count_rooms</span> <b>skirtingas</b> auditorijas su nusiskundimais.</td></tr>";

				echo "<tr><td>Iš jų <span class=badge rel=tooltip2 title='dažniausiai pasikartojanti auditorija'>$most_room</span> auditorija turi <b>daugiausiai nusiskundimu</b> su <span class=badge rel=tooltip2 title='tiek kartų pasikarojo nusiskudimai apie šį kabą'>$room_probs</span> problemomis </td></tr>";

				echo "<tr><td>Bendrai sutdentai yra <b>labiausiai nepatenkinti</b> <span class=badge rel=tooltip2 title='auditorija su daugiausiai like per visas problemas'>$most_total_likes_room[0]</span> kabinetu iš viso bendra svarba - <span class=badge rel=tooltip2 title='gali būti mažiau problemų, bet svarbesnės, labiau nervinančios'>$most_total_likes[0]</span></td></tr>";

				echo "<tr><td><b>Svarbiausia</b> problema yra <span class=badge rel=tooltip2 title='koks auditorija turi most svarbos'>$most_likes_room</span> auditorijoje su <span class=badge rel=tooltip2 title='tiek svarbos turi (like)'>$most_likes</span> likes - <span class=badge rel=tooltip2 title='daugiausiai svarbos turinti problema'>$most_likes_prob</span> </td></tr>";
		echo "</tbody> </table></div>";
		



	}

	include ('php/footer.php');

?>
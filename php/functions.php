<?php


	if ( !empty($_POST['action']) ) {

		switch ($_POST['action']) {

			case 'addtable':

				//creating the actual table FSAZ
				$db->query("CREATE TABLE IF NOT EXISTS FSAZ (id INTEGER PRIMARY KEY, auditorija INTEGER, problema TEXT, svarba INTEGER, sprendimas TEXT, rodyti INTEGER, data TEXT, laikas TEXT, ip INTEGER);");

				//antras table settings
				$db->query("CREATE TABLE IF NOT EXISTS settings (id INTEGER PRIMARY KEY, about TEXT, option TEXT, option2 TEXT);");
				$db->query("INSERT INTO settings (about, option, option2) VALUES ('rusiavimas', 'ORDER BY svarba DESC, id DESC', 'Svarbą');");

				break;

			case 'additem':

				//adding stuff to table FSAZ
				$db->query("INSERT INTO FSAZ (auditorija, problema, svarba, sprendimas, rodyti, data, laikas, ip) VALUES ('303', 'Viskas norma gerai', '8', 'daugiau ten but', '1', '$date', '$time', '$ip');");
				break;

			case 'removeall':

				//drop - deletes table, need to recreate sqlite doenst have truncate
				$db->query("DROP TABLE FSAZ;");
				$db->query("DROP TABLE settings;");
				break;

			case 'removerow':

				if ( !empty($_POST['rowid']) ) {

					$tempId = $_POST['rowid'];
					$db->query("DELETE FROM `FSAZ` WHERE id = $tempId;");

				}	
				break;

			case 'hide':

				if ( !empty($_POST['rowid']) ) {

					$tempId = $_POST['rowid'];

						$tempVal = $_POST['hide'];
						$db->query("UPDATE `FSAZ` SET rodyti = $tempVal WHERE id = $tempId;");
						
				}		
				break;

			case 'upvote':

				if ( !empty($_POST['rowid'])) {

					$tempId = $_POST['rowid'];
					$up = $db->query("SELECT svarba FROM FSAZ WHERE id = $tempId;");

					//need this to get rid of PDE object to normal stuff
					foreach ($up as $row) {
						$old = $row['svarba'];
					}

					$tempVal = $old + 1;

					$db->query("UPDATE FSAZ SET svarba = $tempVal WHERE id = $tempId;");

				}

				break;

			case 'downvote':

				if ( !empty($_POST['rowid'])) {

					$tempId = $_POST['rowid'];
					$up = $db->query("SELECT svarba FROM FSAZ WHERE id = $tempId;");

					//need this to get rid of PDE object to normal stuff
					foreach ($up as $row) {
						$old = $row['svarba'];
					}

					$tempVal = $old - 1;

					$db->query("UPDATE FSAZ SET svarba = $tempVal WHERE id = $tempId;");

				}

				break;

			case 'order':

				$option = $_POST['how'];

				switch ($option) {

					case 'byId':
						
						$order = 'ORDER BY id';
						$db->query("UPDATE settings SET option='$order', option2='Datą'  WHERE id = 1;");
						break;

					case 'byId2':
						
						$order = 'ORDER BY id DESC';
						$db->query("UPDATE settings SET option='$order', option2='Datą (nauj)'  WHERE id = 1;");
						break;

					case 'byLikes':

						$order = 'ORDER BY svarba DESC, id DESC';
						$db->query("UPDATE settings SET option = '$order', option2='Svarbą' WHERE id = 1;");
						break;

					case 'byVisibility':

						$order = 'ORDER BY rodyti DESC, svarba DESC, id DESC';
						$db->query("UPDATE settings SET option = '$order', option2='Rodomumą' WHERE id = 1;");
						break;

					case 'byRoom':

						$order = 'ORDER BY auditorija, svarba DESC, id DESC';
						$db->query("UPDATE settings SET option = '$order', option2='Kabinetus' WHERE id = 1;");
						break;

					
					default:
						#nothin
						break;
				}

				break;

			case 'save':

				$text = $_POST['change'];
				$tempId = $_POST['rowid'];
				$which = $_POST['which'];

				$db->query("UPDATE FSAZ SET '$which' = '$text' WHERE id = $tempId");
				break;

			default:
				# nothing
				break;
		}
	}


?>
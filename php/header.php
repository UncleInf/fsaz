<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>fsaz</title>
        <meta name="description" content="fsaz" />
        <meta name="author" content="Tomas" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
        <link href="css/style.css" rel="stylesheet" media="screen" />
    </head>

    <body>

    <div class="container">

        <nav class="navbar navbar-default">

            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">FSAZ</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="admin.php">admin</a></li>
                        <li><a href="statistics.php">statistics</a></li>
                        <li><a href="javascript:addItem();">add item</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->

        </nav>

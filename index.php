<?php

	include ('php/header.php');
	include ('php/connect.php');
	include ('php/functions.php');


	$result = $db->query('SELECT * FROM FSAZ ORDER BY svarba DESC, id DESC');

	//if we havent created table in admin.php
	if (!$result) {

		echo '<p>Something is wrong with tables... I guess you need to fix it. <br/>';
		echo 'You can configure everything at admin pannel <a href=admin.php>here</a></p>';

	} else {

		//if we can read the db and there is info

		echo '<div class="row"> <div class="col-sm-6">';
		echo '<table class="table table-striped">';
		echo '<thead><tr> <th class=col-sm-1>Auditorija</th> <th>Problema</th> <th class=col-sm-1>Svarba</th> </tr></thead><tbody>';

		foreach ($result as $row) {

			if ($row['rodyti'] == 0) {
				continue;
			}

			echo '<tr>';
			echo "<td>$row[auditorija]</td> <td>$row[problema]</td> <td><span class=badge>$row[svarba]</span> <a href='javascript:vote($row[id]);' class='bwIcon glyphicon glyphicon glyphicon-thumbs-up'></a></td> ";
			echo '</tr>';

		}

		echo '</tbody> </table> </div> </div>';




	}

	include ('php/footer.php');
?>
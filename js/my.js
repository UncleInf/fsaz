
//admin-addtabel. making php POST with js and then reloading
function addTable() {
	$.post("admin.php", {action: "addtable"}, function(){
		location.reload();
	});
}


//ad item to table FSAZ
function addItem() {
	$.post("index.php", {action: "additem"}, function(){
		location.reload();
	});
}


//deleting the table
function removeAll() {
	$.post("index.php", {action: "removeall"}, function(){
		location.reload();
	});
}


//delete row in db
function removeRow(id) {
	$.post("admin.php", {action: "removerow", rowid: id}, function(){
		location.reload();
	});
}


//change the visibility
function hide(id, _value) {

	if (_value == 1) {
		var val = 0;
	} else {
		var val = 1;
	}

	$.post("admin.php", {action: "hide", rowid: id, hide: val}, function(){
		location.reload();
	});
}


$(document).ready(function(){
    $("[rel=tooltip]").tooltip({ placement: 'right'});
    $("[rel=tooltip2]").tooltip({ placement: 'top'});
});


function vote(id) {
	$.post("index.php", {action: "upvote", rowid: id}, function(){
		location.reload();
	});
}

function downvote(id) {
	$.post("index.php", {action: "downvote", rowid: id}, function(){
		location.reload();
	});
}


function onSelectChange(option) {
	$.post("index.php", {action: "order", how: option}, function(){
		location.reload();
	});
}


function save(param, id, text) {
	$.post("admin.php", {action: "save", rowid: id, change: text, which: param}, function(){
		// location.reload();
	});
}


//for content editable stuff
$(document).ready(function() {
	$('.cE').dblclick(function() {
    	$(this).attr('contentEditable', true);
    	$(this).focus();
    });
});


$(document).ready(function() {
	$('.cE').blur(function() {
    	$(this).attr('contentEditable', false);
    });
});

$(document).ready(function() {
	$('.cE').keydown(function (e) {
		if ( (e.ctrlKey && e.keyCode == 13) || (e.keyCode == 27) ) {
   			$(this).blur();
  		}
	});
});

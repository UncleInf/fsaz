<?php 

	include ('php/header.php');
	include ('php/connect.php');
	include ('php/functions.php');


	$admin = 1;

	if (!$admin) {

		echo 'Please <a href=#>login</a> to administer this awesome tool.';

	} else {

		echo '<h3>Admin pannel</h3>';

		$db_tables = $db->query('SELECT name FROM sqlite_master WHERE type = "table"');

		$table_count = 0;
		foreach ($db_tables as $row) {	
			$table_count += 1;
		}

		if (!$table_count > 0) {

			echo '<p>I dont think we found any tables :/ <br/>';
			echo '<a href="javascript:addTable();">Add tables</a></p>'.'<br/>';

		} else {

			//all the script continues here

			$temp = $db->query("SELECT * FROM settings WHERE id = 1");
			$order = $temp->fetch(PDO::FETCH_ASSOC);
			$order_c = $order['option'];
			$order_h = $order['option2'];

			$result = $db->query("SELECT * FROM FSAZ $order_c");

			echo '<p>Tables are created (OK). <br/>';
			echo 'Remove all tables (all info will be lost, not reversable): <a href="javascript:removeAll()">do it!</a></p>'.'<br/>';
			// echo 'Currently selected db: '.'<br/><br/>';


			echo '<div class="panel panel-info"> 
					<div class="panel-heading">Sąrašas &nbsp
						<a rel=tooltip title="statistika" href="statistics.php" class="light glyphicon glyphicon-stats"></a>';

			echo 		"<div class='pull-right sellectBar'>
							<a data-toggle='modal' data-target='#basicModal' href='' class='light space-right glyphicon glyphicon-question-sign'></a>
							Rūšiuoti pagal: 
							<select onchange='onSelectChange(value);' class=selectButton>
								<option selected disabled>--$order_h</option>
								<option value=byLikes>Svarbą</option>
								<option value=byId>Datą</option>
								<option value=byId2>Datą (nauj)</option>
								<option value=byVisibility>Rodomumą</option>
								<option value=byRoom>Kabinetus</option>
							</select>
						</div>
					</div>";
			echo '<table class="table table-striped">';
			echo '<thead>
					<tr>
						<th>#</th>
						<th class=col-sm-1>Auditorija</th>
						<th class=col-sm-4>Problema</th>
						<th class=col-sm-3>Sprendimas</th>
						<th class=col-sm-1>Svarba</th>
						<th class=col-sm-1>Rodoma</th>
						<th class=col-sm-1>Info</th>
						<th class=col-sm-1>Naikinti</th>
					</tr>
				</thead>
				<tbody>';

			foreach ($result as $row) {

				if ($row['rodyti']) {
					$icon = "class='bwIcon glyphicon glyphicon-ok-circle'";
				} else {
					$icon = "class='alertIcon glyphicon glyphicon-eye-close'";
				}

				echo "<tr>
						<td>$row[id]</td>
						<td class=cE onblur=save('auditorija',$row[id],$(this).text()); >$row[auditorija]</td>
						<td class=cE onblur=save('problema',$row[id],$(this).text()); >$row[problema]</td>
						<td class=cE onblur=save('sprendimas',$row[id],$(this).text()) >$row[sprendimas]</td>
						<td>
							<a href='javascript:downvote($row[id]);' class='bwIcon glyphicon glyphicon glyphicon-thumbs-down'></a>
							<span class=badge>$row[svarba]</span>
							<a href='javascript:vote($row[id]);' class='bwIcon glyphicon glyphicon glyphicon-thumbs-up'></a>
						</td>
						<td>
							<a href='javascript:hide($row[id], $row[rodyti]);' $icon ></a>
						</td>
						<td>
							<span class='bwIcon glyphicon glyphicon-info-sign' rel=tooltip title='$row[data] / $row[laikas] from $row[ip]'></span>
						</td>
						<td>
							<a href='javascript:removeRow($row[id]);' class='bwIcon glyphicon glyphicon-remove' ></a>
						</td>";
				echo '</tr>';

			}

			echo '</tbody> </table> </div>';


			
			


		}

	}


	include ('php/help.php');
	include ('php/footer.php');

?>